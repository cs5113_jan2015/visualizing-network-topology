var names,ips;
var node;
var link;
var force;
var num_of_nodes = 1;
var flag = true;
var width,height;
var t;
function count_nodes()
{
	d3.json("nodes.json",function(err,graph)
		{
			num_of_nodes = 	graph.nodes.length;
		});
}
function set_timed_flag()
{
	flag = true;
}
function unset_timed_flag()
{

	setTimeout(function(){ flag = false },5000);
}
function toggle_drag()
{
	if(flag == true)
	{
		flag = false;
		document.getElementById('drag_button').innerHTML="Enable Drag";
		force.stop();
	}
	else
	{
		flag = true;
		document.getElementById('drag_button').innerHTML="Disable Drag";	
		force.start();
	}
}

function check_for_toggle(event)
{
	if(event.charCode == 32)
	{
		toggle_drag();
	}
}

count_nodes();
function main()
{
	if(flag == false)
		toggle_drag();
    var w = document.documentElement.clientWidth,
        h = document.documentElement.clientHeight;
        width = 0.78 * w;
        height = 0.9 * h ; 

    document.getElementById("t").style.width = ""+0.8 * w+"px";
    document.getElementById("t").style.height = ""+0.08 * h+"px";
    document.getElementById("t").style.textAlign = "center";
    document.getElementById("s").style.width = ""+0.2 * w+"px";
    document.getElementById("s").style.height = ""+h+"px";
    document.getElementById("s").style.textAlign = "center";



    var r = 0.008*w + 0.008*h;
    if(r>84)
        r= 84;
    var dist = 4*r;
    var prev_node = null;
    var prev_links = new Array();
    var color = d3.scale.category20();
    var num_of_nodes = 1;

    force = d3.layout.force()
    	.charge(-720*num_of_nodes*4)
        .linkDistance(dist)
        .size([width, height]);

    function redraw(){
      svg.attr("transform","translate(" + d3.event.translate + ")"+ " scale(" + d3.event.scale + ")");
    }

    var svg = d3.select("#graph").append("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("pointer-events", "all")
        .call(d3.behavior.zoom().scaleExtent([1,8]).on("zoom", redraw)).on("dblclick.zoom", null);

    d3.json("nodes.json", function(error, graph) {
      force
          .nodes(graph.nodes)
          .links(graph.links)
          .start();

      link = svg.selectAll(".link")
          .data(graph.links)
        .enter().append("line")
          .attr("class", "link")
          .style("stroke-width", 4.8);


      node = svg.selectAll(".node")
      	  .data(graph.nodes)
          .enter().append("circle")
          .attr("class", "node")
          .attr("r", r)
          .attr("unselectable","on")
          .style("fill", function(d) { return color(d.group); })
          .on("mousedown",function(d){
  
            var info_bar=document.getElementById("info_bar");
            info_bar.innerHTML="<br><br><br><br>System Name: "+d.name+"<br><br>IP Address: "+d.ip+"<br><br>Number of nodes connected: "+d.count+"<br><br>Location: "+d.location+"<br><br>Interfaces: "+"<br>";
            
            if(d.interfaces.length != 0)
            {
	            for (intf in d.interfaces)
	            {
	            	info_bar.innerHTML+="<br>Interface Name: " + d.interfaces[intf].description + "<br>MAC: " + d.interfaces[intf].physicalAddress + "<br>Speed: " + d.interfaces[intf].speed +" bits/sec<br>Type: "+d.interfaces[intf].type+"<br>";
	            }
	        }
	        else
	        {
	        	info_bar.innerHTML+="<br>Unknown";
	        }
            if(prev_node != null)
                prev_node.style("stroke","");
            d3.select(this).style("stroke", "red");
            prev_node = d3.select(this);
            var ind;
            for(ind=0;ind < prev_links.length;ind++)
            {
                link[0][prev_links[ind]].style.stroke = "#444";
                link[0][prev_links[ind]].style.strokeOpacity = "0.5";
            }
            prev_links.length = 0;
            for(ind = 0;ind < link[0].length;ind++)
            {
                if((link[0][ind].getAttribute("from") == d.index) || (link[0][ind].getAttribute("to") == d.index))
                {
                    link[0][ind].style.stroke = "#ffff00";
                    link[0][ind].style.strokeOpacity = "1";
                    prev_links.push(ind);
                }
            }
          })
          .call(force.drag);

      var texts1 = svg.selectAll("text.label")
            .data(graph.nodes)
            .enter().append("text")
            .attr("class", "name_label")
            .attr("fill", "black")
            .attr("style","font-size:"+0.2*r+"px")
            .text(function(d) {
                return d.name;
            });
        var texts2 = svg.selectAll("text.label")
            .data(graph.nodes)
            .enter().append("text")
            .attr("class", "ip_label")
            .attr("fill", "black")
            .attr("style","font-size:"+0.2*r+"px")
            .text(function(d) {
                return d.ip;
            });


      force.on("tick", function() {
      	if(flag == false)
        {
        	return;	
     	}

        link.attr("x1", function(d) { return d.source.x ; })
            .attr("y1", function(d) { return d.source.y ; })
            .attr("x2", function(d) { return d.target.x ; })
            .attr("y2", function(d) { return d.target.y ; })
            .attr("from",function(d){ return d.source.index ;})
            .attr("to",function(d){ return d.target.index ;});

        node.attr("cx", function(d) { return d.x; })
            .attr("cy", function(d) { return d.y; });

       texts1.attr("x", function(d) { return d.x ; })
             .attr("y", function(d) { return d.y - r/3; }); 
        texts2.attr("x", function(d) { return d.x; })
              .attr("y", function(d) { return d.y + r/3 ; });


      });
    });

}
function resize()
{
    document.getElementById("graph").innerHTML="";
    main();
}
main();
