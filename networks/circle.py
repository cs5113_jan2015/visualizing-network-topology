#NOTE: This file produces a sample graph
from Graph import Graph
from Node import Node
import random
import sys
import math
import json

graph=Graph()

n=0

if len(sys.argv)<2:
	print "Enter number of nodes"
	n=int(raw_input())
else:
	n=int(sys.argv[1])

for i in range(n):
	node=Node(i, "S"+str(i), "192.168.0."+str(i), "12:34:56:78:90:"+str(i), 0)
	graph.add_node(node)

for i in range(n):
	graph.add_link(i, (i+1)%n, random.randrange(50, 100))

print json.dumps(graph.get_json())