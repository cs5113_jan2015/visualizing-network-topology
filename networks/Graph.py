from Node import Node

class Graph:
	def __init__(self):
		self.nodes=[]
		self.links=dict()

	def add_node(self, node):
		self.nodes.append(node)

	def add_link(self, id1, id2, weight):
		if weight<=0 or not (0<=id1<len(self.nodes) and 0<=id2<len(self.nodes)):
			return None
		if id1>id2:
			self.add_link(id2, id1, weight)
		elif (id1, id2) not in self.links:
			self.links[(id1, id2)]=weight

	def num_nodes(self):
		return len(self.nodes)

	def get_node(self, _id):
		if _id<self.num_nodes:
			return self.nodes[_id]
		return None

	def get_link_count(self, _id):
		return len(
			filter(
				lambda pair: pair[0]==_id or pair[1]==_id
				, list(self.links.keys())
				)
			)

	def is_connected(self, id1, id2):
		if id1==id2:
			return True
		if id1>id2:
			return self.is_connected(id2, id1)
		return (id1, id2) in self.links

	def get_json(self):
		nodes=[]
		for node in self.nodes:
			nodes.append(node.get_dict(link_count=self.get_link_count(node.get_id())))
		links=[]
		for link in self.links:
			links.append({"source": link[0], "target": link[1], "value": self.links[link]})

		return {"nodes": nodes, "links": links}