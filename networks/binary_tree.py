#NOTE: This file produces a sample graph
from Graph import Graph
from Node import Node
import random
import sys
import math
import json

graph=Graph()

n=0

if len(sys.argv)<2:
	print "Enter number of nodes"
	n=int(raw_input())
else:
	n=int(sys.argv[1])

for i in range(n):
	group=int(math.log(i+1, 2))
	node=Node(i, "S"+str(i), "192.168."+str(group)+"."+str(i), "12:34:56:78:90:"+str(i), group)
	graph.add_node(node)

for i in range(n/2):
	graph.add_link(i, 2*i+1, random.randrange(50, 100))
	graph.add_link(i, 2*(i+1), random.randrange(50, 100))

print json.dumps(graph.get_json())