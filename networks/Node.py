
class Node:
	def __init__(self, _id, name, ip, group, interfaces, location=None):
		self.id=_id
		self.name=name
		self.ip=ip
		self.group=group
		self.location=location
		self.interfaces=interfaces
	def get_id(self):
		return self.id
	def get_ip(self):
		return self.ip
	def get_name(self):
		return self.name
	def get_dict(self, link_count=None):
		ans=dict()
		ans["index"]=self.id
		if self.name is not None:
			ans["name"]=self.name
		if self.ip is not None:
			ans["ip"]=self.ip
		if self.group is not None:
			ans["group"]=self.group
		if self.interfaces is not None:
			ans["interfaces"]=self.interfaces
		if self.location is not None:
			ans["location"]=self.location
		if link_count is not None:
			ans["count"]=link_count
		return ans
	def get_group(self):
		return self.group
	def __str__(self):
		return self.get_dict()