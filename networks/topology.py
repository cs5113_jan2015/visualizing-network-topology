import subprocess
import sys
import json
import re
import os
import ConfigParser
from Node import Node
from Graph import Graph
from Queue import Queue

#reading the config file for version and authentication details
cfg_file='topology.cfg'

if not os.path.isfile(cfg_file):
	print 'Config file ('+cfg_file+') missing.'
	exit()

cfg=ConfigParser.ConfigParser()
cfg.read(cfg_file)


COMMAND="snmpwalk"
VERSION="3"

try:
	#getting parameters from the configuration file
	USER=cfg.get('credentials', 'username')
	LEVEL=cfg.get('credentials', 'level')
	PASSPHRASE=cfg.get('credentials', 'passphrase')
except ConfigParser.NoOptionError as e:
	print >> sys.stderr, e
	print >> sys.stderr, "Please fix the configuration file"
	exit()

NEIGHBOR_OID="SNMPv2-SMI::mib-2.3.1.1.3.32.1"
NAME_OID="SNMPv2-MIB::sysName"
LOCATION_OID="SNMPv2-MIB::sysLocation"
IF_OIDS={
	"index": "IF-MIB::ifIndex",
	"description": "IF-MIB::ifDescr",
	"type": "IF-MIB::ifType",
	"speed": "IF-MIB::ifSpeed",
	"physicalAddress": "IF-MIB::ifPhysAddress"
	}

#Important variables to be used
ip_id_map=dict()
graph=Graph()
done=set()
locations=dict()

def is_valid_ip(ip):
	m=re.match(r"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$", ip)
	return bool(m) and all(map(lambda n: 0 <= int(n) <= 255, m.groups()))

def get_command(ip, oid):
	return [COMMAND,"-v", VERSION, "-u", USER, "-l", LEVEL, "-A", PASSPHRASE, str(ip), oid]

def get_neighbor_list(ip):
	command=get_command(ip, NEIGHBOR_OID)
	proc=subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	if proc.wait()==0:
		out, err=proc.communicate()
		out=out.split('\n')
		return filter(is_valid_ip, map(extract_last, out))
	else:
		return list()

def get_name(ip):
	command=get_command(ip, NAME_OID)
	proc=subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	if proc.wait()==0:
		out, err=proc.communicate()
		return out.split("\n")[0].split("STRING: ")[-1]
	return None

def get_location(ip):
	command=get_command(ip, LOCATION_OID)
	proc=subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	if proc.wait()==0:
		out, err=proc.communicate()
		return out.split("\n")[0].split("STRING: ")[-1]
	return None

def get_interfaces_info(ip, OID):
	command=get_command(ip, OID)
	proc=subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	if proc.wait()==0:
		out, err=proc.communicate()
		out=out.split("\n")[:-1]
		keys=map(lambda row: row.split(' ', 2)[0][len(OID)+1:], out)
		values=map(extract_last, out)
		return dict(zip(keys, values))
	else:
		return dict()

def set_interfaces_info(ip, interfaces, key, convert=str):
	data=get_interfaces_info(ip, IF_OIDS[key])
	for i in data:
		if i in interfaces:
			interfaces[i][key]=convert(data[i])

def get_interfaces(ip):
	interfaces=dict()

	#contruct indices
	command=get_command(ip, IF_OIDS["index"])
	proc=subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	if proc.wait()==0:
		out, err=proc.communicate()
		out=map(extract_last, out.split("\n")[:-1])
		for x in out:
			interfaces[x]={"index": int(x)}
	else:
		return interfaces

	set_interfaces_info(ip, interfaces, "description")
	set_interfaces_info(ip, interfaces, "type")
	set_interfaces_info(ip, interfaces, "speed", int)
	set_interfaces_info(ip, interfaces, "physicalAddress")

	return interfaces

def extract_last(result):
	return result.split(' ')[-1]

def get_node_id(ip):
	if ip in ip_id_map:
		return ip_id_map[ip]

	#If not present, create a node and add it
	return create_node(ip).get_id()


def create_node(ip):
	global node_count

	print "Creating node for", ip

	name=get_name(ip)

	if name is None:
		name="unknown"
		location="unknown"
		interfaces=dict()
	else:
		location=get_location(ip) or "unknown"
		interfaces=get_interfaces(ip)

	if location in locations:
		group=locations[location]
	else:
		locations[location]=group=len(locations)

	node=Node(graph.num_nodes(), name, ip, group, interfaces.values(), location)
	ip_id_map[ip]=node.get_id()
	graph.add_node(node)
	return node


def main():
	start=sys.argv[1]

	#initialize queue
	queue=Queue()

	if is_valid_ip(start):
		queue.put(create_node(start))
	else:
		with open(start, 'r') as in_file:
			for line in in_file:
				line=line.strip()
				if is_valid_ip(line):
					queue.put(graph.get_node(get_node_id(line)))
				else:
					print >> sys.stderr, "Invalid IP address in file. Please fix and try again"
					exit()

	while not queue.empty():
		cur_node=queue.get()
		if cur_node is not None and (cur_node.get_name()=="unknown" or cur_node.get_id() in done):
			continue

		print "Finding neighbors of", cur_node.get_ip()
		connected_list=get_neighbor_list(cur_node.get_ip())
		for neighbor_ip in connected_list:
			neighbor_id=get_node_id(neighbor_ip)
			weight=1 #default

			if cur_node.get_id()!=neighbor_id:	#check if self link
				graph.add_link(cur_node.get_id(), neighbor_id, weight)

			if neighbor_id not in done:
				queue.put(graph.get_node(neighbor_id))

		done.add(cur_node.get_id())

	if len(sys.argv)>2:
		outfile=open(sys.argv[2], 'w')
	else:
		outfile=None

	if outfile is None:
		print json.dumps(graph.get_json(), indent=1)
	else:
		json.dump(graph.get_json(), outfile, indent=1)

	

if __name__ == '__main__':
	main()
