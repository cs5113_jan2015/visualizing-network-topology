# Visualizing Network Topology #

This program uses SNMP to construct a network topology using the Breadth-First Search algorithm.

### Configuration file ###
The program requires a configuration file by the name `topology.cfg` in the directory of the program to run. The format of the configuration file is as follows:

```ini
[credentials]
username=security_name
level=security_level
passphrase=authentication_protocol_pass_phrase
```

### Inputs ###

* Single or a list of IP address of the starting nodes
* Name of the output file

### Output Format ###

The program outputs the graph of the topology in **json** format. The attributes in the **json** are as follows:

```javascript
{
"nodes": [
    {
    "index": Integer,
    "name": String,
    "ip": String,
    "count": Integer,
    "location": String,
    "group": Integer,
    "interfaces": [
        {
        "index": Integer,
        "type": String,
        "description": String,
        "physicalAddress": String,
        "speed": Integer
        },
        ...
        ]
    },
    ...
    ],
"links": [
    {
    "source": Integer,
    "target": Integer,
    "value" Integer
    },
    ...
    ]
}
```
### Execution ###

Type in the following command to run it

```bash
python topology.py {ip_address|in_file} [out_file]
```

| input | Description |
|:------:|:----------------|
| ip_address  | Starting IP address |
| in_file | File containing list of starting IP addresses |
| out_file | The **json** file in which the output will be written. If this file is not specified, the output is dumped on the *stdout* |

### Visualization ###
Open the file `index.html` to display an interactive visualization of the network topology. The visualization is built using the [d3.js](http://d3js.org) javascript library.

### Contact ###

* [Abdul Aziz](mailto: cs12b1001@iith.ac.in)
* [Agam Agarwal](mailto: cs12b1003@iith.ac.in)
* [Samrat Phatale](mailto: cs12b1035@iith.ac.in)
